# docs2web Configuration Files

This repository contains configuration files for various PDFs and DOCX files related to TTRPG games. Please always purchase any PDFs you wish to convert, and remember unless you have publishing rights to the original documents, the converted output is for private reference and personal use ONLY. :)

## File Prefix Legend

* **SR5** - Shadow Run, 5th Edition
* **SWADE** - Savage Worlds Adventure Edition
* **W40kWG** - Warhammer 40k - Wrath and Glory
